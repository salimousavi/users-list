const COLUMNS = ['id', 'name', 'username', 'email', 'address']

function makeRequest (method, url) {
    return new Promise(function (resolve, reject) {
        var xhr = new XMLHttpRequest();
        xhr.open(method, url);
        xhr.onload = function () {
            if (this.status >= 200 && this.status < 300) {
                resolve(JSON.parse(xhr.response));
            } else {
                reject({
                    status: this.status,
                    statusText: xhr.statusText
                });
            }
        };
        xhr.onerror = function () {
            reject({
                status: this.status,
                statusText: xhr.statusText
            });
        };
        xhr.send();
    });
}


makeRequest('GET', 'https://jsonplaceholder.typicode.com/users')
    .then(users => renderPage(users))
    .catch(error => {
        console.log(error)
        throw `Cann't get data`
    })

class User {
    constructor({id, name, username, email, address: _address}) {
        this.id = id
        this.name = name
        this.username = username
        this.email = email
        this._address = _address
    }

    get address() {
        return `${this._address.city} ${this._address.street} ${this._address.suite}`
    }

    remove() {
        document.getElementById(`tr-${this.id}`).remove()
    }

    render() {
        const tr = document.createElement('tr')
        tr.setAttribute('id', `tr-${this.id}`)

        COLUMNS.forEach(element => {
            const td = document.createElement('td')

            td.textContent = this[element]

            tr.appendChild(td)
        })

        const action = document.createElement('td')
        action.textContent = 'x'
        action.style.color = 'red'
        action.style.cursor = 'pointer'
        action.style.padding = '5px'
        action.addEventListener('click', el => this.remove())

        tr.appendChild(action)

        return tr
    }
}

function renderPage(users) {
    const root = document.getElementById('root')

    const table = document.createElement('table')
    table.setAttribute('border', 1)
    table.setAttribute('width', '100%')
    table.appendChild(renderTableHead())

    users.forEach(el => {
        const user = new User(el)
        table.appendChild(user.render())
    })

    root.appendChild(renderHead())
    root.appendChild(table)
}

function renderHead() {
    const h2 = document.createElement('h2')
    h2.textContent = 'Users list'

    return h2
}

function renderTableHead() {
    const tr = document.createElement('tr')

    COLUMNS.forEach(element => {
        const th = document.createElement('th')
        th.textContent = element
        tr.appendChild(th)
    })

    return tr
}